<?php

namespace app\ENUMs;

class AnimalStatusEnum
{
	const Healthy="Healthy";
	const CannotWalk="Cannot walk";
	const Dead="Dead";
}