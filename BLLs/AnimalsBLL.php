<?php

namespace app\BLLs;

use app\models\Animal;
use app\models\Elephant;
use app\models\Giraffe;
use app\models\Monkey;

class AnimalsBLL
{
	const MONKEY_DEATH_HEALTH_LEVEL = 30;
	const GIRAFFE_DEATH_HEALTH_LEVEL = 50;
	const ELEPHANT_CANNOT_WALK_HEALTH_LEVEL = 70;
	const ELEPHANT_CANNOT_WALK_COUNT_MAX = 2;

	public static function createAnimals()
	{
		$animalList = [];
		$animalList[Elephant::TYPE] = self::createElephant(5);
		$animalList[Monkey::TYPE] = self::createMonkeys(5);
		$animalList[Giraffe::TYPE] = self::createGiraffe(5);
		return $animalList;
	}

	public static function ElapseHour($animalList)
	{
		foreach ($animalList as $animals) {
			/** @var Animal $animal */
			foreach ($animals as $animal) {
				$healthToDecrease = rand(0, 20);
				$animal->decreaseHealth($healthToDecrease);
			}
		}
		return $animalList;
	}

	public static function FeedAnimals($animalList)
	{
		foreach ($animalList as $animals) {
			$healthToAdd = rand(10, 25);
			/** @var Animal $animal */
			foreach ($animals as $animal) {
				$animal->addHealth($healthToAdd);
			}
		}
		return $animalList;
	}

	private static function createElephant($quantity)
	{
		$list = [];
		for ($i = 0; $i < $quantity; $i++) {
			$list[] = new Elephant(self::ELEPHANT_CANNOT_WALK_HEALTH_LEVEL, self::ELEPHANT_CANNOT_WALK_COUNT_MAX);
		}
		return $list;
	}

	private static function createMonkeys($quantity)
	{
		$list = [];
		for ($i = 0; $i < $quantity; $i++) {
			$list[] = new Monkey(Monkey::TYPE, self::MONKEY_DEATH_HEALTH_LEVEL);
		}
		return $list;
	}

	private static function createGiraffe($quantity)
	{
		$list = [];
		for ($i = 0; $i < $quantity; $i++) {
			$list[] = new Monkey(Giraffe::TYPE, self::GIRAFFE_DEATH_HEALTH_LEVEL);
		}
		return $list;
	}
}