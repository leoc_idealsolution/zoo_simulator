<?php

namespace app\models;

use app\ENUMs\AnimalStatusEnum;

class Animal
{
	const INITIAL_HEALTH_LEVEL = 100;
	const TYPE = "animal";

	protected $health;
	protected $deathHealthLevel;
	protected $type;

	public function __construct($type = self::TYPE, $deathHealthLevel = 0)
	{
		$this->health = self::INITIAL_HEALTH_LEVEL;
		$this->deathHealthLevel = $deathHealthLevel;
		$this->isDead = false;
		$this->type = $type;
	}

	public function isDead()
	{
		return $this->health <= $this->deathHealthLevel;
	}

	public function addHealth($amount)
	{
		if ($this->health + $amount < self::INITIAL_HEALTH_LEVEL) {
			$this->health += $amount;
		} else {
			$this->health = self::INITIAL_HEALTH_LEVEL;
		}
	}

	public function decreaseHealth($amount)
	{
		if ($this->health - $amount > 0) {
			$this->health -= $amount;
		} else {
			$this->health = 0;
		}
	}

	public function currentStatus()
	{
		if ($this->isDead()) {
			return AnimalStatusEnum::Dead;
		}
		return AnimalStatusEnum::Healthy;
	}

	/**
	 * @return int
	 */
	public function getHealth()
	{
		return $this->health;
	}
}