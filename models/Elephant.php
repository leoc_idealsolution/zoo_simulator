<?php

namespace app\models;

use app\ENUMs\AnimalStatusEnum;

class Elephant extends Animal
{
	const TYPE = "elephant";

	protected $cannotWalkHealthLevel;
	protected $cannotWalkCount;
	protected $cannotWalkCountMax;


	public function __construct($cannotWalkHealthLevel, $cannotWalkCountMax)
	{
		parent::__construct(self::TYPE);
		$this->cannotWalkHealthLevel = $cannotWalkHealthLevel;
		$this->cannotWalkCount = 0;
		$this->cannotWalkCountMax = $cannotWalkCountMax;
	}

	public function isCannotWalk()
	{
		return $this->health < $this->cannotWalkHealthLevel;
	}

	public function isDead()
	{
		return $this->isCannotWalk() && ($this->cannotWalkCount >= $this->cannotWalkCountMax);
	}

	private function increaseCannotWalkCount()
	{
		$this->cannotWalkCount += 1;
	}

	private function resetCannotWalkCount()
	{
		$this->cannotWalkCount = 0;
	}

	public function addHealth($amount)
	{
		if ($this->health + $amount < self::INITIAL_HEALTH_LEVEL) {
			$this->health += $amount;
		} else {
			$this->health = self::INITIAL_HEALTH_LEVEL;
		}

		if ($this->health >= $this->cannotWalkHealthLevel) {
			$this->resetCannotWalkCount();
		}
	}

	public function decreaseHealth($amount)
	{
		if ($this->health - $amount > 0) {
			$this->health -= $amount;
		} else {
			$this->health = 0;
		}

		if ($this->health < $this->cannotWalkHealthLevel) {
			$this->increaseCannotWalkCount();
		}
	}

	public function currentStatus()
	{
		if ($this->isDead()) {
			return AnimalStatusEnum::Dead;
		}
		if ($this->isCannotWalk()) {
			return AnimalStatusEnum::CannotWalk;
		}
		return AnimalStatusEnum::Healthy;
	}
}