<?php

/* @var $this yii\web\View
 * @var $animals
 * @var $time
 */

use yii\helpers\Html;

$this->title = 'Zoo Simulator';
?>

Current time:
<?= $time ?>
<table class="table table-striped">
	<tr>
		<th>
			Animal
		</th>
		<th>
			Health level
		</th>
		<th>
			Status
		</th>
	</tr>
	<?php foreach ($animals as $type => $animalList): ?>
		<?php foreach ($animalList as $animal): ?>
			<tr>
				<td><?= $type ?></td>
				<td><?= $animal->getHealth() ?>%</td>
				<td><?= $animal->currentStatus() ?></td>
			</tr>

		<?php endforeach; ?>
	<?php endforeach; ?>
</table>
<?= Html::button('Elapse 1 hour', ['class' => 'btn btn-primary', 'onclick' => 'window.location = \'/index.php?r=site/elapse\'']); ?>
<?= Html::button('Feed Animals', ['class' => 'btn btn-success', 'onclick' => 'window.location = \'/index.php?r=site/feed\'']); ?>
