<?php

namespace app\controllers;

use app\BLLs\AnimalsBLL;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\HttpException;

class SiteController extends Controller
{
	const SESSION_KEY_ANIMALS = "animals";
	const SESSION_KEY_TIME = "time";

	public function __construct($request, $emitter)
	{
		parent::__construct($request, $emitter);
		$session = Yii::$app->session;
		if (!$session->get(self::SESSION_KEY_ANIMALS)) {
			$animals = AnimalsBLL::createAnimals();
			$session->set(self::SESSION_KEY_ANIMALS, $animals);
			$session->set(self::SESSION_KEY_TIME, date('H:i:s'));
		}
	}

	public function actionIndex()
	{
		$session = Yii::$app->session;
		if (!$session->get(self::SESSION_KEY_ANIMALS)) {
			throw new HttpException("Error to initialize animals");
		}
		return $this->render('index', [
			'animals' => $session->get(self::SESSION_KEY_ANIMALS),
			'time' => $session->get(self::SESSION_KEY_TIME)
		]);
	}

	public function actionElapse()
	{
		$session = Yii::$app->session;
		if (!$session->get(self::SESSION_KEY_ANIMALS)) {
			throw new HttpException("Error to initialize animals");
		}
		$animals = AnimalsBLL::ElapseHour($session->get(self::SESSION_KEY_ANIMALS));
		$session->set(self::SESSION_KEY_ANIMALS, $animals);
		$time = date('H:i:s', strtotime($session->get(self::SESSION_KEY_TIME) . ' +1 hour'));
		$session->set(self::SESSION_KEY_TIME, $time);
		$this->redirect('/index.php');
	}

	public function actionFeed()
	{
		$session = Yii::$app->session;
		if (!$session->get(self::SESSION_KEY_ANIMALS)) {
			throw new HttpException("Error to initialize animals");
		}
		$animals = AnimalsBLL::FeedAnimals($session->get(self::SESSION_KEY_ANIMALS));
		$session->set(self::SESSION_KEY_ANIMALS, $animals);
		$this->redirect('/index.php');
	}

}
